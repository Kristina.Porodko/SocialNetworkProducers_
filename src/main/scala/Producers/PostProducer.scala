package Producers

import java.util.Properties

import Entities.Post
import Generators.PostGenerator
import com.sksamuel.avro4s.RecordFormat
import org.apache.kafka.clients.producer.KafkaProducer

import scala.util.Random

class PostProducer[K, V] extends Producer{

  def this(topic: String, properties: Properties) {
    this()
    this.topic = topic
    this.properties = properties
    producer = new KafkaProducer[Any, AnyRef](properties)
  }

  override def run(): Unit = {
    try {
      while (!Thread.currentThread.isInterrupted) {
        val instantActionsCount = 5 + Random.nextInt(3);
        for (_ <- 0 until instantActionsCount) {
          var postGenerator = new PostGenerator()
          var post = postGenerator.generate()
          val format = RecordFormat[Post]
          val schemedPost = format.to(post)

          produce(post.postid, schemedPost)
        }

        Thread.sleep(1000)
      }
    } catch {
      case ex: Throwable =>
        this.close()
        println(ex.getMessage)
    } finally {

    }
  }

}

