package Producers

import java.util.Properties

import Entities.Action
import Generators.ActionGenerator
import com.sksamuel.avro4s.RecordFormat
import org.apache.kafka.clients.producer.KafkaProducer

import scala.util.Random

class ActionProducer[K, V] extends Producer{

  def this(topic: String, properties: Properties){
    this()
    this.topic = topic
    this.properties = properties
    producer = new KafkaProducer[Any, AnyRef](properties)
  }

  override def run(): Unit = {
    try {
      while (!Thread.currentThread.isInterrupted) {
        val instantActionsCount = 3 + Random.nextInt(3); //From 3 to 5 actions per second
        for (_ <- 0 until instantActionsCount) {
          var actionid = Random.nextInt(3000)
          var actionGenerator = new ActionGenerator()
          var action = actionGenerator.generate()
          val format = RecordFormat[Action]
          val schemedAction = format.to(action)

          produce(actionid, schemedAction)
        }

        Thread.sleep(1000)
      }
    } catch {
      case ex: Throwable =>
        //this.close()
        println(ex.getMessage)
    } finally {

    }
  }

}
