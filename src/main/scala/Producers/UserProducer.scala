package Producers

import java.util.Properties

import Entities.User
import com.sksamuel.avro4s.RecordFormat
import org.apache.kafka.clients.producer.KafkaProducer

import scala.io.Source

class UserProducer[K, V] extends Producer{
  val sourceFilepath = "data/users.txt"
  val formatter: RecordFormat[User] = RecordFormat[User]

  def this(topic: String, properties: Properties){
    this()
    this.topic = topic
    this.properties = properties
    producer = new KafkaProducer[Any, AnyRef](properties)
  }

  override def run(): Unit = {
    try {
      Source.fromFile(sourceFilepath).getLines().foreach(line => {
        val values: Array[String] = line.split("\t")

        if (values.length != 5) {
          throw new IllegalArgumentException("Incorrect file content!")
        }

        val id = values(0).toInt
        val name = values(1)
        val surname = values(2)
        val city = values(3)
        val country = values(4)

        val user = User(id, name, surname, city, country)
        val formattedUser = formatter.to(user)
        produce(id, formattedUser)
      })

    } catch {
      case ex: Throwable =>
        this.close()
        println(ex.getMessage)

    }
  }

}
