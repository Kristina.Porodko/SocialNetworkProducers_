package Producers

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}


abstract class Producer[K, V] extends Runnable {
  //private val kafkaProps : Properties = new Properties()
  //private var config : ProducerConfig = null

  protected var producer : KafkaProducer [Any, AnyRef] = null
  protected var topic : String = null
  protected var properties : Properties = null

 /* def configure(brokerList : String, syncType : String): Unit ={

    val batchSize = 16384
    kafkaProps.put("bootstrap.servers", brokerList)
    kafkaProps.put("producer.type", syncType)
    kafkaProps.put("key.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer")
    kafkaProps.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer")
    kafkaProps.put("schema.registry.url", "http://localhost:8081/") // config.schema
    kafkaProps.put("acks", "all")
    kafkaProps.put("batch.size", 16834)

    config = new ProducerConfig(kafkaProps)
  } */

  def produce(key: Any, value: AnyRef): Unit ={
    //TODO: get topic from some config settings
    val producerRecord = new ProducerRecord[Any, AnyRef](topic, key, value)
    producer.send(producerRecord)
  }

  def close(): Unit ={
    producer.flush()
    producer.close()
  }
  def run()
}
