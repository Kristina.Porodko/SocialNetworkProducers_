package argsparser

case class Config(brokerList : String = "localhost:9092",
                  topic : String = "users",
                  sync : String = "async",
                  delay : Long = 1000,
                  count : Integer = 10
                 )
