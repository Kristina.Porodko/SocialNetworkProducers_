package argsparser

import scopt.OptionParser

object Parser {
  var coreObj : OptionParser[Config] = null;

  private def create() : OptionParser[Config] = {
    new OptionParser[Config]("SocoalNetworkProducers") {
      head("SocoalNetworkProducers", "0.x")
      note("allows to produce random users and push them to chosen topic")

      opt[String]('b', "brokerList").action((x, c) =>
        c.copy(brokerList = x)).text("list of brokers - network nodes")

      opt[String]('t', "topic").
        action((x, c) => c.copy(topic = x)).
        text("name of topic the producer will push messages to")

      //            opt[String]('a', "age").
      //                    action( (x, c) => c.copy(age = x) ).
      //                    text("type of the producer (old or new)")

      opt[String]('s', "sync").valueName("<sync type (sync/async)>")
        .action((x, c) => c.copy(sync = x))
        .validate(x =>
          if (x == "sync" || x == "async") success
          else failure("Wrong sync type!")
        )
        .text("delay between produced messages");

      opt[Long]('d', "delay").valueName("<time in milliseconds>")
        .action((x, c) => c.copy(delay = x))
        .validate(x =>
          if (x >= 0) success
          else failure("Delay can not be less than 0!")
        )
        .text("delay between produced messages");

      opt[Int]('c', "count").valueName("<value>")
        .action((x, c) => c.copy(count = x))
        .validate(x =>
          if (x >= 0) success
          else failure("Messages count can not be less than 0!")
        )
        .text("amount of messages to produce");

    }
  }

  def core() : OptionParser[Config] = {
    if(coreObj == null) {
      coreObj = create();
    }
    coreObj;
  }

  def parse(args : Array[String]) : Config = {
    core().parse(args, Config()) match {
      case Some(config) =>
        config
      case None =>
        null
      //throw new IllegalArgumentException("Cannot parse the arguments!");
    }
  }
}


