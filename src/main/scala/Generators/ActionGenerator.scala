package Generators

import Entities.Action
import scala.util.Random

class ActionGenerator {
  //TODO: Read data from file
  // TODO: Create mechanism for producing integral data
  val actionNames = List("Like", "Dislike", "Repost")

  def generate() : Action = {
    var userId = Random.nextInt(1000)
    var postId = Random.nextInt(2000)
    var actionid = Random.nextInt(3000)
    var actionNameIndex = Random.nextInt(actionNames.length)

    val action = Action(userId, postId, actionNames(actionNameIndex))
    action;
  }
}

