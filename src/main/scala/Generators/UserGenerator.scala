package Generators

import scala.util.Random

import Entities.User
import java.io.File

class UserGenerator {
  val names = List("Jaimy Lannister", "Jaimy's Hand", "Keily Hevy", "Sam Song")
  val surnames = List("Taity", "Denim", "Aurilia", "Rosy", "Vazon")
  val cities = List("Stavanger", "Oslo", "Lviv", "Kyiv", "CitName")
  val countries = List("Ukraine", "Norway", "Poland", "Great Britain")


   def generate(): User ={
    //TODO : forecast method for generating user's id
    var id = Random.nextInt(1000)
    var nameIndex = Random.nextInt(names.length)
    var surnameIndex = Random.nextInt(surnames.length)
    var cityIndex = Random.nextInt(cities.length)
    var countryIndex = Random.nextInt(countries.length)

    val user = User(id, names(nameIndex), surnames(surnameIndex), cities(cityIndex), countries(countryIndex))
    println(user.name)
    user;
//    //TODO : This data should be produced to topic
//    val os = AvroOutputStream.data[User](new File("users.avro"))
//    os.write(user)
//    os.flush()
//    os.close()


  }
}
