package Generators

import scala.util.Random

import Entities.Post

class PostGenerator {
  val contents = List("Yes. that's it!", "I like snow!", "Yushchenko nash Presydent!")

  def generate() : Post = {
    var postId = Random.nextInt(2000)
    var userId = Random.nextInt(1000)
    var contentIndex = Random.nextInt(contents.length)

    val post = Post(postId, userId, contents(contentIndex))
    post
  }
}


