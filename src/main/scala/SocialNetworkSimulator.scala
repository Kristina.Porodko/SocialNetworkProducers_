package myMainClass

import java.util.Properties

import Producers.{ActionProducer, PostProducer, UserProducer}

import com.google.common.io.Resources

object SocialNetworkSimulator {

  def main(args: Array[String]): Unit = {
    val producerProps = configureProducerProps()

    val userProducer = new UserProducer("users", producerProps)
    val postProducer = new PostProducer("posts", producerProps)
    val actionProducer = new ActionProducer("actions", producerProps)

    val userProducerThread = new Thread(userProducer)
    val postProducerThread = new Thread(postProducer)
    val actionProducerThread = new Thread(actionProducer)

    userProducerThread.start()
    postProducerThread.start()
    actionProducerThread.start()
  }


  private def configureProducerProps(): Properties ={
    val props = Resources.getResource("producer.properties")
                          .openStream()
    val producerProps = new Properties()
    producerProps.load(props)

    producerProps
  }

}


